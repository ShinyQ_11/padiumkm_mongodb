FROM node:16.3.0
RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app
COPY . .

RUN npm install

EXPOSE 9000

CMD ["npm", "start"]

