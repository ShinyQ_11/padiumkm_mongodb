var chai = require("chai");
var chaiHttp = require("chai-http");
var expect = chai.expect;
var server = require("../../../index.js");

chai.should();
chai.use(chaiHttp);


describe('API Testing', () => {

    it('should check if the app is running', (done) => {
      chai.request(server)
          .get("/")
          .end((err, res) => {
            res.should.have.status(200);
          done();
          });
    });

    it('should post an item to database', (done) => {
      const item = {
        "id": "1",
        "name": "Alat Pertanian",
        "brand": "Caterpillar",
        "available_stock": "20",
        "price": "300000"
      };
      chai.request(server)
          .post('/api/v1/item/')
          .send(item)
          .end((err, res) => {
            res.should.have.status(201);
            res.body.data.should.have.property('name');
          done();
          });
    });
    
    it('should give status error 400 when post an item with name less than 3 character', (done) => {
      const item = {
        "id": "3",
        "name": "ab",
        "brand": "Caterpillar",
        "available_stock": "20",
        "price": "400000"
      };
      chai.request(server)
          .post('/api/v1/item/')
          .send(item)
          .end((err, res) => {
            res.should.have.status(400);
          done();
          });
    });

    it('should give status error 400 when post an item with wrong price number format', (done) => {
      const item = {
        "id": "3",
        "name": "Traktor",
        "brand": "Caterpillar",
        "available_stock": "20",
        "price": "40000,0"
      };
      chai.request(server)
          .post('/api/v1/item/')
          .send(item)
          .end((err, res) => {
            res.should.have.status(400);
          done();
          });
    });

    it('should give status error 404 when modified an item with undefined id', (done) => {
      const id = 21
      const item = {
        "name": "Traktor",
        "brand": "Caterpillar",
        "price": "400000"
      };
      chai.request(server)
          .patch('/api/v1/item/' + id)
          .send(item)
          .end((err, res) => {
            res.should.have.status(404);
          done();
          });
    });
  });