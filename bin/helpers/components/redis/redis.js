const sentryLog = require('../sentry/sentry_log');

const redis = require("redis");

const redisConnection = async () => {  
    let redisClient = redis.createClient({
        host:"127.0.0.1",
        port:"6379",
        db:0
    });

    redisClient.on('error', (err) => console.log('Redis Client Error', err))

    try {
        await redisClient.connect()
        return redisClient
    } catch (error) {
        sentryLog.sendError(error)
        return wrapper.error(`Connection failed`,``, 500);
    }
}

const store = async (name, data) => {  
    let redisClient = await redisConnection()
    try {
        await redisClient.set(name, data)
    } catch (error) {
        sentryLog.sendError(error)
        return wrapper.error(`Connection failed`,``, 500);
    }
}

const get = async (name) => {  
    let redisClient = await redisConnection()
    try {
        const user = await redisClient.get(name)
        return user
    } catch (error) {
        sentryLog.sendError(error)
        return wrapper.error(`Connection failed`,``, 500);
    }
}

module.exports = {
    store:store,
    get:get
}