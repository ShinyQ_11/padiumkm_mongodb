'use strict';

const restify = require('restify');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const sentryLog = require('../helpers/components/sentry/sentry_log');
const corsMiddleware = require('restify-cors-middleware');

//Services Handler
const transactionHandler = require('../modules/transaction/handlers/api_handler');
const addressHandler = require('../modules/address/handlers/api_handler');
const userHandler = require('../modules/user/handlers/api_handler');
const itemHandler = require('../modules/item/handlers/api_handler');
const categoryHandler = require('../modules/category/handlers/api_handler');
const reviewHandler = require('../modules/review/handlers/api_handler');
const roleHandler = require('../modules/role/handlers/api_handler');

let crossOrigin = (req,res,next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    return next();
}

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
    allowHeaders: ['Origin, X-Requested-With, Content-Type, Accept, OPTIONS'],
    exposeHeaders: ['OPTIONS']
})

let AppServer = function(){
    this.server = restify.createServer({
        name: project.name + '-server',
        version: project.version
    });

    this.server.serverKey = '';
    this.server.pre(cors.preflight);
    this.server.use(cors.actual);
    this.server.use(restify.plugins.acceptParser(this.server.acceptable));
    this.server.use(restify.plugins.queryParser());
    this.server.use(restify.plugins.bodyParser());
    this.server.use(restify.plugins.authorizationParser());

    //required for basic auth
    this.server.use(basicAuth.init());
    this.server.use(crossOrigin);

    //anonymous can access the end point, place code bellow
    this.server.get('/', (req, res, next) => {
        wrapper.response(res,`success`,wrapper.data(`Index`),`This service is running properly`);
    });
    
    // Add Sentry Logger
    this.server.get("api/v1/debug-sentry", function mainHandler(req, res) {
        sentryLog.sendError("Test Sentry")
    });

    //authenticated client can access the end point, place code bellow

    //User
    this.server.post('api/v1/user/', userHandler.postOneUser);
    this.server.get('api/v1/user/list', userHandler.getUser);
    this.server.get('api/v1/user/cache', userHandler.postCacheUser);
    this.server.get('api/v1/user/cached', userHandler.getCacheUser);
    this.server.get('api/v1/user', userHandler.getOneUser);
    this.server.post('api/v1/user/login', userHandler.postDataLogin);

    //Transaction
    this.server.post('api/v1/transaction/', transactionHandler.postOneTransaction);
    this.server.get('api/v1/transaction/', transactionHandler.getAllTransactions);
    this.server.get('api/v1/transaction/:id', transactionHandler.getOneTransaction);
    this.server.del('api/v1/transaction/:id', transactionHandler.deleteOneTransaction);
    this.server.patch('api/v1/transaction/:id', transactionHandler.patchOneTransaction);

    //Address
    this.server.post('api/v1/address/', addressHandler.postOneAddress);
    this.server.get('api/v1/address/', addressHandler.getAllAddresses);
    this.server.get('api/v1/address/:id', addressHandler.getOneAddress);
    this.server.del('api/v1/address/:id', addressHandler.deleteOneAddress);
    this.server.patch('api/v1/address/:id', addressHandler.patchOneAddress);

    //Item
    this.server.post('api/v1/item/', itemHandler.postOneItem);
    this.server.get('api/v1/item/', itemHandler.getAllItems);
    this.server.get('api/v1/item/:id', itemHandler.getOneItem);
    this.server.del('api/v1/item/:id', itemHandler.deleteOneItem);
    this.server.patch('api/v1/item/:id', itemHandler.patchOneItem);

    //Category
    this.server.post('api/v1/category/', categoryHandler.postOneCategory);
    this.server.get('api/v1/category/', categoryHandler.getAllCategories);
    this.server.get('api/v1/category/:id', categoryHandler.getOneCategory);
    this.server.del('api/v1/category/:id', categoryHandler.deleteOneCategory);
    this.server.patch('api/v1/category/:id', categoryHandler.patchOneCategory);

    //Category
    this.server.post('api/v1/role/', roleHandler.postOneRole);
    this.server.get('api/v1/role/', roleHandler.getAllRoles);
    this.server.get('api/v1/role/:id', roleHandler.getOneRole);
    this.server.del('api/v1/role/:id', roleHandler.deleteOneRole);
    this.server.patch('api/v1/role/:id', roleHandler.patchOneRole);

    //Review
    this.server.post('api/v1/review/', reviewHandler.postOneReview);
    this.server.get('api/v1/review/', reviewHandler.getAllReviews);
    this.server.get('api/v1/review/:id', reviewHandler.getOneReview);
    this.server.del('api/v1/review/:id', reviewHandler.deleteOneReview);
    this.server.patch('api/v1/review/:id', reviewHandler.patchOneReview);
   

}
 
module.exports = AppServer;