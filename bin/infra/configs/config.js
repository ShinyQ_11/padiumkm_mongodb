'use strict';

module.exports.initEnvironments = (nconf) => {
  nconf.env().file('config.json');
  nconf.defaults({
    'PORT':3000,
    'PUBLIC_KEY_PATH': 'T3Lk0M',
    'PRIVATE_KEY_PATH': 'T3Lk0M'
  });
};