'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Category = require('./domain');

const getOneCategory = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const category = new Category(queryParam);
        const result = await category.viewOneCategory();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}


const getAllCategories = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const category = new Category(queryParam);
        const result = await category.viewAllCategories();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}


module.exports = {
    getOneCategory : getOneCategory,
    getAllCategories : getAllCategories
}