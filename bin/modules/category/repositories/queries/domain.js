'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");

class Category{
    constructor(param){
        this.id = param.id,
        this.category_name = param.category_name;
        this.createdAt =  param.createdAt,
        this.updatedAt = param.updatedAt
    }

    async viewOneCategory(){
        const param = {"id": this.id};
        const result = await query.findOneCategory(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }


    async viewAllCategories(){
        const param = {};
        const result = await query.findAllCategories(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

}

module.exports = Category;