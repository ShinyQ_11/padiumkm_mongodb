'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findOneCategory = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('category');
    const recordset = await db.findOne(parameter);
    return recordset;
}

const findAllCategories = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('category');
    const recordset = await db.findMany();
    return recordset;
}


module.exports = {
    findOneCategory: findOneCategory,
    findAllCategories: findAllCategories
}