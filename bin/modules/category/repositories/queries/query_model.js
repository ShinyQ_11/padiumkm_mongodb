'use strict';

const generalItem = () => {
    const model = {
        id:``,
        category_name: ``,
        createdAt:``,
        updatedAt:``,      
    }
    return model;
}

module.exports = {
    generalItem: generalItem
}
