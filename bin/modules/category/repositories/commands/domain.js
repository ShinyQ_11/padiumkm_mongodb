'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./command_model');
const command = require('./command');
const query = require('../queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const SNS = require('../../../../helpers/components/aws-sns/sns');
const Emitter = require('../../../../helpers/events/event_emitter');
const EventPublisher = require('../../../../helpers/events/event_publisher');

class Category{

    async addNewCategory(payload){
        const data = [payload];
        let view = model.generalCategory();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.category_name)){accumulator.category_name = value.category_name;}     
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneCategory(document);
        return result;
    }

    async updateCategory(params, payload){
        const data = [payload];
        let view = model.generalCategory();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.category_name)){accumulator.category_name = value.category_name;}       
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneCategory(params, document);
        return result;
    }

    async deleteCategory(params){
        const result = await command.deleteOneCategory(params);
        return result;
    }

}

module.exports = Category;