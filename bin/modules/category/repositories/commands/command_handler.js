'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Category = require('./domain');

const postOneCategory = async (payload) => {
    const category = new Category();
    const postCommand = async (payload) => {
        return await category.addNewCategory(payload);
    }
    return postCommand(payload);
}

const patchOneCategory = async (id, payload) => {
    const category = new Category();
    const putCommand = async (id, payload) => {
        return await category.updateCategory(id, payload);
    }
    return putCommand(id, payload);
}

const deleteOneCategory = async (id) => {
    const category = new Category();
    const delCommand = async (id) => {
        return await category.deleteCategory(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneCategory : postOneCategory,
    patchOneCategory : patchOneCategory,
    deleteOneCategory : deleteOneCategory
}