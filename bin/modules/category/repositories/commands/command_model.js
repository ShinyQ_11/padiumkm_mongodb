'use strict';

const generalCategory = () => {
    const model = {
        id:``,
        category_name: ``,
        createdAt:``,
        updatedAt:``,    
    }
    return model;
}

module.exports = {
    generalCategory: generalCategory
}
