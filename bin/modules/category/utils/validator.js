'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneCategory = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.category_name] = {length: {minimum: 5}};
    values[payload.category_name] = payload.category_name;

    return await validateConstraints(values,constraints);
}

const isValidParamGetAllCategories = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.category_name] = {length: {minimum: 5}};
    values[payload.category_name] = payload.category_name;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneCategory = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.category_name] = {length: {minimum: 5}};
    values[payload.category_name] = payload.category_name;
    return await validateConstraints(values,constraints);
}

const ifExistCategory = async (payload) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('address');
    console.log('payload:');
    console.log(payload);
    const parameter = {"id": payload.id};
    const result = await db.findOne(parameter);
    return result;
}

module.exports = {
    isValidParamGetOneCategory: isValidParamGetOneCategory,
    isValidParamGetAllCategories: isValidParamGetAllCategories,
    isValidParamPostOneCategory: isValidParamPostOneCategory,
    ifExistCategory: ifExistCategory,
}