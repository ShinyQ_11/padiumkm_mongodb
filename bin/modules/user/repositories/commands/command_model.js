'use strict';

const generalUser = () => {
    const model = {
        name:``,
        email:``,
        password:``,
        created_at: Date.now(),
        updated_at: Date.now()   
    }
    return model;
}

module.exports = {
    generalUser: generalUser
}
