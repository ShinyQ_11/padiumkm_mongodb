'use strict';

const generalMockup = () => {
    const model = {
        projectName:``,
        domainName:``,
        apiName:``,
        mockup:{},
        createdAt:``,
        createdBy:``,
        updatedAt:``,
        updatedBy:``      
    }
    return model;
}

module.exports = {
    generalMockup: generalMockup
}
