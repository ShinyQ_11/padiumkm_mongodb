'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const redisService = require('../../../../helpers/components/redis/redis')
class User{

    async viewAllUser(){
        const user = await query.findAllUser();
        const { data } = user

        if(user.err){
            return wrapper.error('error', 'Can not find user!', 404);
        }

        return wrapper.data(data, '', 200);
    }

    async viewCacheUser(){
        const data = await redisService.get('user');
        return wrapper.data(JSON.parse(data), '', 200);
    }

    async viewOneUser(){
        const user = await query.findOneUser();
        const { data } = user

        if(user.err){
            return wrapper.error('error', 'Can not find user!', 404);
        }
        
        return wrapper.data(data, '', 200);
    }
}

module.exports = User;