'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const User = require('./domain');

const getAllUser = async () => {
    const getData = async () => {
        const user = new User();
        const result = await user.viewAllUser();
        return result;
    }
    const result = await getData();
    return result;
}

const getCacheUser = async () => {
    const getData = async () => {
        const user = new User();
        const result = await user.viewCacheUser();
        return result;
    }
    const result = await getData();
    return result;
}

const getOneUser = async (userId) => {
    const getData = async () => {
        const user = new User();
        const result = await user.viewOneUser();
        return result;
    }

    const result = await getData();
    return result;
}

module.exports = {
    getAllUser,
    getOneUser,
    getCacheUser
}