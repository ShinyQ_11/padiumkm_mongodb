'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamPostOneUser = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    constraints[payload.password] = {length: {minimum: 5}};
    constraints[payload.email] = {length: {minimum: 5}};
    constraints[payload.roleID] = {};
    
    values[payload.name] = payload.name;
    values[payload.email] = payload.email;
    values[payload.password] = payload.password;
    values[payload.roleId] = payload.roleId;

    return await validateConstraints(values,constraints);
}

module.exports = {
    isValidParamPostOneUser: isValidParamPostOneUser,
}