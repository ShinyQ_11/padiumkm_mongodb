'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator')
const jwtAuth = require('../../../auth/jwt_auth_helper');
const redisService = require('../../../helpers/components/redis/redis')

const postDataLogin = async (req, res, next) => {
  const payload = req.body;
  const postRequest = async () => {
    return await commandHandler.postDataLogin(payload);
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest());
}

const getUser = async (req, res, next) => {
  const getData = async () => {
    return await queryHandler.getAllUser();
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await getData());
}

const getOneUser = async (req, res, next) => {
  const getData = async () => {
    const userId = jwtAuth.verifyToken(req)
    return await queryHandler.getOneUser(userId);
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await getData());
}

const postOneUser = async (req, res, next) => {
  const payload = req.body;

  const validateParam = await validator.isValidParamPostOneUser(payload);
  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.postDataRegister(payload);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest(validateParam));
}

const postCacheUser = async (req, res, next) => {
  const getData = async () => {
    return await queryHandler.getAllUser();
  }
  const sendResponse = async (result) => {
    try {
      await redisService.store('user', JSON.stringify(result.data))
      (result.err) ? wrapper.response(res,'fail',result) : 
      wrapper.response(res,'success', [],`Your Request Has Been Processed`);
    } catch (error) {
      return wrapper.error(`Connection failed`,``, 500);
    }
  }

  sendResponse(await getData());
}

const getCacheUser = async (req, res, next) => {
  const getData = async () => {
    return await queryHandler.getCacheUser();
  }
  const sendResponse = async (result) => {
    console.log(result);
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success', result,`Your Request Has Been Processed`);
  }

  sendResponse(await getData());
}

module.exports = {
  postOneUser,
  postDataLogin,
  postCacheUser,
  getUser,
  getOneUser,
  getCacheUser
}