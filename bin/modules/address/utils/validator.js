'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneAddress = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    constraints[payload.receiver_name] = {length: {minimum: 2}};
    constraints[payload.receiver_phone_number] = {length: {minimum: 10}};
    constraints[payload.region] = {length: {minimum: 4}};
    constraints[payload.city] = {length: {minimum: 4}};
    constraints[payload.zipcode] = {length: {minimum: 4}};
    constraints[payload.full_address] = {length: {minimum: 4}};
    values[payload.name] = payload.name;
    values[payload.receiver_name] = payload.receiver_name;
    values[payload.receiver_phone_number] = payload.receiver_phone_number;
    values[payload.region] = payload.region;
    values[payload.city] = payload.city;
    values[payload.zipcode] = payload.apiName;
    values[payload.full_address] = payload.full_address;
    return await validateConstraints(values,constraints);
}

const isValidParamGetAllAddresses = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    constraints[payload.receiver_name] = {length: {minimum: 2}};
    constraints[payload.receiver_phone_number] = {length: {minimum: 10}};
    constraints[payload.region] = {length: {minimum: 4}};
    constraints[payload.city] = {length: {minimum: 4}};
    constraints[payload.zipcode] = {length: {minimum: 4}};
    constraints[payload.full_address] = {length: {minimum: 4}};
    values[payload.name] = payload.name;
    values[payload.receiver_name] = payload.receiver_name;
    values[payload.receiver_phone_number] = payload.receiver_phone_number;
    values[payload.region] = payload.region;
    values[payload.city] = payload.city;
    values[payload.zipcode] = payload.apiName;
    values[payload.full_address] = payload.full_address;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneAddress = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    constraints[payload.receiver_name] = {length: {minimum: 2}};
    constraints[payload.receiver_phone_number] = {length: {minimum: 10}};
    constraints[payload.region] = {length: {minimum: 4}};
    constraints[payload.city] = {length: {minimum: 4}};
    constraints[payload.zipcode] = {length: {minimum: 4}};
    constraints[payload.full_address] = {length: {minimum: 4}};
    values[payload.name] = payload.name;
    values[payload.receiver_name] = payload.receiver_name;
    values[payload.receiver_phone_number] = payload.receiver_phone_number;
    values[payload.region] = payload.region;
    values[payload.city] = payload.city;
    values[payload.zipcode] = payload.apiName;
    values[payload.full_address] = payload.full_address;
    return await validateConstraints(values,constraints);
}

const ifExistAddress = async (payload) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('address');
    console.log('payload:');
    console.log(payload);
    const parameter = {"id": payload.id};
    const result = await db.findOne(parameter);
    return result;
}

module.exports = {
    isValidParamGetOneAddress: isValidParamGetOneAddress,
    isValidParamGetAllAddresses: isValidParamGetAllAddresses,
    isValidParamPostOneAddress: isValidParamPostOneAddress,
    ifExistAddress: ifExistAddress,
}