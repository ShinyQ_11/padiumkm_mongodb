'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./command_model');
const command = require('./command');
const query = require('../queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const SNS = require('../../../../helpers/components/aws-sns/sns');
const Emitter = require('../../../../helpers/events/event_emitter');
const EventPublisher = require('../../../../helpers/events/event_publisher');

class Address{

    async addNewAddress(payload){
        const data = [payload];
        let view = model.generalAddress();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.name)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.receiver_name)){accumulator.receiver_name = value.receiver_name;}
            if(!validate.isEmpty(value.receiver_phone_number)){accumulator.receiver_phone_number = value.receiver_phone_number;}
            if(!validate.isEmpty(value.region)){accumulator.region = value.region;}
            if(!validate.isEmpty(value.city)){accumulator.city = value.city;}   
            if(!validate.isEmpty(value.zipcode)){accumulator.zipcode = value.zipcode;}   
            if(!validate.isEmpty(value.full_address)){accumulator.full_address = value.full_address;}           
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneAddress(document);
        return result;
    }

    async updateAddress(params, payload){
        const data = [payload];
        let view = model.generalAddress();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.name)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.receiver_name)){accumulator.receiver_name = value.receiver_name;}
            if(!validate.isEmpty(value.receiver_phone_number)){accumulator.receiver_phone_number = value.receiver_phone_number;}
            if(!validate.isEmpty(value.region)){accumulator.region = value.region;}
            if(!validate.isEmpty(value.city)){accumulator.city = value.city;}   
            if(!validate.isEmpty(value.zipcode)){accumulator.zipcode = value.zipcode;}   
            if(!validate.isEmpty(value.full_address)){accumulator.full_address = value.full_address;}          
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneAddress(params, document);
        return result;
    }

    async deleteAddress(params){
        const result = await command.deleteOneAddress(params);
        return result;
    }

}

module.exports = Address;