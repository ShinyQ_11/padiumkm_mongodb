'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Address = require('./domain');

const postOneAddress = async (payload) => {
    const address = new Address();
    const postCommand = async (payload) => {
        return await address.addNewAddress(payload);
    }
    return postCommand(payload);
}

const patchOneAddress = async (id, payload) => {
    const address = new Address();
    const putCommand = async (id, payload) => {
        return await address.updateAddress(id, payload);
    }
    return putCommand(id, payload);
}

const deleteOneAddress = async (id) => {
    const address = new Address();
    const delCommand = async (id) => {
        return await address.deleteAddress(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneAddress : postOneAddress,
    patchOneAddress : patchOneAddress,
    deleteOneAddress : deleteOneAddress
}