'use strict';

const generalAddress = () => {
    const model = {
        id:``,
        name: ``,
        receiver_name: ``,
        receiver_phone_number: ``,
        region: ``,
        city: ``,
        zipcode: ``,
        full_address: ``,
        createdAt:``,
        updatedAt:``,    
    }
    return model;
}

module.exports = {
    generalAddress: generalAddress
}
