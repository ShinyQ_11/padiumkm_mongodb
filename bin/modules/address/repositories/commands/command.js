'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneAddress = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('address');
    const result = await db.insertOne(document);
    return result;
}

const updateOneAddress = async (params, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('address');
    const result = await db.upsertOne(params, document);
    return result;
}

const deleteOneAddress = async (params) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('address');
    const result = await db.deleteOne(params);
    return result;
}

module.exports = {
    insertOneAddress: insertOneAddress,
    updateOneAddress: updateOneAddress,
    deleteOneAddress: deleteOneAddress
}