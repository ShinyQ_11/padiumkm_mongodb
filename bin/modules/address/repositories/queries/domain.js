'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");

class Address{
    constructor(param){
        this.id = param.id,
        this.name = param.name;
        this.receiver_name = param.receiver_name;
        this.receiver_phone_number = param.receiver_phone_number;
        this.region = param.region;
        this.city = param.city;
        this.zipcode = param.zipcode;
        this.full_address = param.full_address;
        this.createdAt =  param.createdAt,
        this.updatedAt = param.updatedAt
    }

    async viewOneAddress(){
        const param = {"id": this.id};
        const result = await query.findOneAddress(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }


    async viewAllAddresses(){
        const param = {};
        const result = await query.findAllAddresses(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

}

module.exports = Address;