'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Address = require('./domain');

const getOneAddress = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const address = new Address(queryParam);
        const result = await address.viewOneAddress();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}


const getAllAddresses = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const address = new Address(queryParam);
        const result = await address.viewAllAddresses();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}


module.exports = {
    getOneAddress : getOneAddress,
    getAllAddresses : getAllAddresses
}