'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");

class Item{
    constructor(param){
        this.id = param.id,
        this.name = param.name;
        this.brand = param.brand;
        this.available_stock = param.available_stock;
        this.price = param.price;
        this.createdAt =  param.createdAt,
        this.updatedAt = param.updatedAt
    }

    async viewOneItem(){
        const param = {"id": this.id};
        const result = await query.findOneItem(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }


    async viewAllItems(){
        const param = {};
        const result = await query.findAllItems(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

}

module.exports = Item;