'use strict';

const generalItem = () => {
    const model = {
        id:``,
        name: ``,
        brand: ``,
        available_stock: ``,
        price: ``,
        createdAt:``,
        updatedAt:``,      
    }
    return model;
}

module.exports = {
    generalItem: generalItem
}
