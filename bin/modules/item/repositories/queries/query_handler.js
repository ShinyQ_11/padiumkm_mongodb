'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Item = require('./domain');

const getOneItem = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const item = new Item(queryParam);
        const result = await item.viewOneItem();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}


const getAllItems = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const item = new Item(queryParam);
        const result = await item.viewAllItems();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}


module.exports = {
    getOneItem : getOneItem,
    getAllItems : getAllItems
}