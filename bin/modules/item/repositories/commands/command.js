'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneItem = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('item');
    const result = await db.insertOne(document);
    return result;
}

const updateOneItem = async (params, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('item');
    const result = await db.upsertOne(params, document);
    return result;
}

const deleteOneItem = async (params) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('item');
    const result = await db.deleteOne(params);
    return result;
}

module.exports = {
    insertOneItem: insertOneItem,
    updateOneItem: updateOneItem,
    deleteOneItem: deleteOneItem
}