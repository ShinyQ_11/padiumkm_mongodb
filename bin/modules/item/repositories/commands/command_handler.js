'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Item = require('./domain');

const postOneItem = async (payload) => {
    const item = new Item();
    const postCommand = async (payload) => {
        return await item.addNewItem(payload);
    }
    return postCommand(payload);
}

const patchOneItem = async (id, payload) => {
    const item = new Item();
    const putCommand = async (id, payload) => {
        return await item.updateItem(id, payload);
    }
    return putCommand(id, payload);
}

const deleteOneItem = async (id) => {
    const item = new Item();
    const delCommand = async (id) => {
        return await item.deleteItem(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneItem : postOneItem,
    patchOneItem : patchOneItem,
    deleteOneItem : deleteOneItem
}