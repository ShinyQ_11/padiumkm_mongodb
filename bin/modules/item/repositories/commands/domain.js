'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./command_model');
const command = require('./command');
const query = require('../queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const SNS = require('../../../../helpers/components/aws-sns/sns');
const Emitter = require('../../../../helpers/events/event_emitter');
const EventPublisher = require('../../../../helpers/events/event_publisher');

class Item{

    async addNewItem(payload){
        const data = [payload];
        let view = model.generalItem();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.name)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.brand)){accumulator.brand = value.brand;}
            if(!validate.isEmpty(value.available_stock)){accumulator.available_stock = value.available_stock;}
            if(!validate.isEmpty(value.price)){accumulator.price = value.price;}        
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneItem(document);
        return result;
    }

    async updateItem(params, payload){
        const data = [payload];
        let view = model.generalItem();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.name)){accumulator.name = value.name;}
            if(!validate.isEmpty(value.brand)){accumulator.brand = value.brand;}
            if(!validate.isEmpty(value.available_stock)){accumulator.available_stock = value.available_stock;}
            if(!validate.isEmpty(value.price)){accumulator.price = value.price;}         
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneItem(params, document);
        return result;
    }

    async deleteItem(params){
        const result = await command.deleteOneItem(params);
        return result;
    }

}

module.exports = Item;