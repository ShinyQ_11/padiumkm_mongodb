'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneItem = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    constraints[payload.brand] = {length: {minimum: 2}};
    constraints[payload.available_stock] = {length: {minimum: 1}};
    constraints[payload.price] = {length: {minimum: 4}};
    values[payload.name] = payload.name;
    values[payload.brand] = payload.brand;
    values[payload.available_stock] = payload.available_stock;
    values[payload.price] = payload.price;

    return await validateConstraints(values,constraints);
}

const isValidParamGetAllItems = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    constraints[payload.brand] = {length: {minimum: 2}};
    constraints[payload.available_stock] = {length: {minimum: 1}};
    constraints[payload.price] = {length: {minimum: 4}};
    values[payload.name] = payload.name;
    values[payload.brand] = payload.brand;
    values[payload.available_stock] = payload.available_stock;
    values[payload.price] = payload.price;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneItem = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 3}};
    constraints[payload.brand] = {length: {minimum: 2}};
    constraints[payload.available_stock] = {length: {minimum: 1}};
    constraints[payload.price] = {
        numericality: {
            onlyInteger: true
        }};
    values[payload.name] = payload.name;
    values[payload.brand] = payload.brand;
    values[payload.available_stock] = payload.available_stock;
    values[payload.price] = payload.price;
    return await validateConstraints(values,constraints);
}

const ifExistItem = async (payload) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('item');
    const parameter = {"id": payload.id};
    const result = await db.findOne(parameter);
    return result;
}

module.exports = {
    isValidParamGetOneItem: isValidParamGetOneItem,
    isValidParamGetAllItems: isValidParamGetAllItems,
    isValidParamPostOneItem: isValidParamPostOneItem,
    ifExistItem: ifExistItem,
}