'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneRole = async (document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('roles');
    const result = await db.insertOne(document);
    return result;
}

const updateOneRole = async (params, document) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('roles');
    const result = await db.upsertOne(params, document);
    return result;
}

const deleteOneRole = async (params) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('roles');
    const result = await db.deleteOne(params);
    return result;
}

module.exports = {
    insertOneRole: insertOneRole,
    updateOneRole: updateOneRole,
    deleteOneRole: deleteOneRole
}