'use strict';

const Role = require('./domain');

const postOneRole = async (payload) => {
    const role = new Role();
    const postCommand = async (payload) => {
        return await role.addNewRole(payload);
    }
    return postCommand(payload);
}

const patchOneRole = async (id, payload) => {
    const role = new Role();
    const putCommand = async (id, payload) => {
        return await role.updateRole(id, payload);
    }
    return putCommand(id, payload);
}

const deleteOneRole = async (id) => {
    const role = new Role();
    const delCommand = async (id) => {
        return await role.deleteRole(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneRole : postOneRole,
    patchOneRole : patchOneRole,
    deleteOneRole : deleteOneRole
}