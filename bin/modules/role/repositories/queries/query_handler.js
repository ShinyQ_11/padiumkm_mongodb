'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Role = require('./domain');

const getOneRole = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const category = new Role(queryParam);
        const result = await category.viewOneRole();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}


const getAllRoles = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const category = new Role(queryParam);
        const result = await category.viewAllRoles();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}


module.exports = {
    getOneRole : getOneRole,
    getAllRoles : getAllRoles
}