'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneRole = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    values[payload.name] = payload.name;

    return await validateConstraints(values,constraints);
}

const isValidParamGetAllCategories = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    values[payload.name] = payload.name;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneRole = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    values[payload.name] = payload.name;
    return await validateConstraints(values,constraints);
}

const ifExistRole = async (payload) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('roles');

    const parameter = {"id": payload.id};
    const result = await db.findOne(parameter);
    return result;
}

module.exports = {
    isValidParamGetOneRole: isValidParamGetOneRole,
    isValidParamGetAllCategories: isValidParamGetAllCategories,
    isValidParamPostOneRole: isValidParamPostOneRole,
    ifExistRole: ifExistRole,
}