'use strict';

const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findOneReview = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('review');
    const recordset = await db.findOne(parameter);
    return recordset;
}

const findAllReviews = async (parameter) => {
    parameter = {$and:[parameter]};
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('review');
    const recordset = await db.findMany();
    return recordset;
}


module.exports = {
    findOneReview: findOneReview,
    findAllReviews: findAllReviews
}