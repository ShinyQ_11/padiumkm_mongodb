'use strict';

const generalItem = () => {
    const model = {
        id:``,
        item_id: ``,
        user_id: ``,
        review: ``,
        createdAt:``,
        updatedAt:``,      
    }
    return model;
}

module.exports = {
    generalItem: generalItem
}
