'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Review = require('./domain');

const getOneReview = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const review = new Review(queryParam);
        const result = await review.viewOneReview();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}


const getAllReviews = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const review = new Review(queryParam);
        const result = await review.viewAllReviews();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}


module.exports = {
    getOneReview : getOneReview,
    getAllReviews : getAllReviews
}