'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Review = require('./domain');

const postOneReview = async (payload) => {
    const review = new Review();
    const postCommand = async (payload) => {
        return await review.addNewReview(payload);
    }
    return postCommand(payload);
}

const patchOneReview = async (id, payload) => {
    const review = new Review();
    const putCommand = async (id, payload) => {
        return await review.updateReview(id, payload);
    }
    return putCommand(id, payload);
}

const deleteOneReview = async (id) => {
    const review = new Review();
    const delCommand = async (id) => {
        return await review.deleteReview(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneReview : postOneReview,
    patchOneReview : patchOneReview,
    deleteOneReview : deleteOneReview
}