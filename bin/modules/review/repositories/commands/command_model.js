'use strict';

const generalReview = () => {
    const model = {
        id:``,
        item_id: ``,
        user_id: ``,
        review: ``,
        createdAt:``,
        updatedAt:``,    
    }
    return model;
}

module.exports = {
    generalReview: generalReview
}
