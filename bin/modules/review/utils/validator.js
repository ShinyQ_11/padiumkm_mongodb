'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneReview = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.item_id] = {length: {minimum: 1}};
    constraints[payload.user_id] = {length: {minimum: 1}};
    constraints[payload.review] = {length: {minimum: 10}};
    values[payload.item_id] = payload.item_id;
    values[payload.user_id] = payload.user_id;
    values[payload.review] = payload.review;

    return await validateConstraints(values,constraints);
}

const isValidParamGetAllReviews = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.item_id] = {length: {minimum: 1}};
    constraints[payload.user_id] = {length: {minimum: 1}};
    constraints[payload.review] = {length: {minimum: 10}};
    values[payload.item_id] = payload.item_id;
    values[payload.user_id] = payload.user_id;
    values[payload.review] = payload.review;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneReview = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.item_id] = {length: {minimum: 1}};
    constraints[payload.user_id] = {length: {minimum: 1}};
    constraints[payload.review] = {length: {minimum: 10}};
    values[payload.item_id] = payload.item_id;
    values[payload.user_id] = payload.user_id;
    values[payload.review] = payload.review;
    return await validateConstraints(values,constraints);
}

const ifExistReview = async (payload) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('review');
    console.log('payload:');
    console.log(payload);
    const parameter = {"id": payload.id};
    const result = await db.findOne(parameter);
    return result;
}

module.exports = {
    isValidParamGetOneReview: isValidParamGetOneReview,
    isValidParamGetAllReviews: isValidParamGetAllReviews,
    isValidParamPostOneReview: isValidParamPostOneReview,
    ifExistReview: ifExistReview,
}