'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const Mongo = require('../../../helpers/databases/mongodb/db');
const MySQL = require('../../../helpers/databases/mysql/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneTransaction = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.item_id] = {length: {minimum: 1}};
    constraints[payload.user_id] = {length: {minimum: 1}};
    constraints[payload.address_id] = {length: {minimum: 1}};
    constraints[payload.quantity] = {length: {minimum: 1}};
    constraints[payload.total_price] = {length: {minimum: 1}};
    constraints[payload.payment_method] = {length: {minimum: 1}};
    values[payload.item_id] = payload.item_id;
    values[payload.user_id] = payload.user_id;
    values[payload.address_id] = payload.address_id;
    values[payload.quantity] = payload.quantity;
    values[payload.total_price] = payload.quantity;
    values[payload.payment_method] = payload.quantity;

    return await validateConstraints(values,constraints);
}

const isValidParamGetAllTransactions = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.item_id] = {length: {minimum: 1}};
    constraints[payload.user_id] = {length: {minimum: 1}};
    constraints[payload.address_id] = {length: {minimum: 1}};
    constraints[payload.quantity] = {length: {minimum: 1}};
    constraints[payload.total_price] = {length: {minimum: 1}};
    constraints[payload.payment_method] = {length: {minimum: 1}};
    values[payload.item_id] = payload.item_id;
    values[payload.user_id] = payload.user_id;
    values[payload.address_id] = payload.address_id;
    values[payload.quantity] = payload.quantity;
    values[payload.total_price] = payload.quantity;
    values[payload.payment_method] = payload.quantity;
    return await validateConstraints(values,constraints);
}

const isValidParamPostOneTransaction = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.item_id] = {length: {minimum: 1}};
    constraints[payload.user_id] = {length: {minimum: 1}};
    constraints[payload.address_id] = {length: {minimum: 1}};
    constraints[payload.quantity] = {length: {minimum: 1}};
    constraints[payload.total_price] = {length: {minimum: 1}};
    constraints[payload.payment_method] = {length: {minimum: 1}};
    values[payload.item_id] = payload.item_id;
    values[payload.user_id] = payload.user_id;
    values[payload.address_id] = payload.address_id;
    values[payload.quantity] = payload.quantity;
    values[payload.total_price] = payload.quantity;
    values[payload.payment_method] = payload.quantity;
    return await validateConstraints(values,constraints);
}

const ifExistTransaction = async (payload) => {
    const db = new Mongo(config.getDevelopmentDB());
    db.setCollection('item');
    console.log('payload:');
    console.log(payload);
    const parameter = {"id": payload.id};
    const result = await db.findOne(parameter);
    return result;
}

module.exports = {
    isValidParamGetOneTransaction: isValidParamGetOneTransaction,
    isValidParamGetAllTransactions: isValidParamGetAllTransactions,
    isValidParamPostOneTransaction: isValidParamPostOneTransaction,
    ifExistTransaction: ifExistTransaction,
}