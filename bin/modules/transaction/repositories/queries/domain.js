'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./query_model');
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");

class Transaction{
    constructor(param){
        this.id = param.id,
        this.item_id = param.item_id;
        this.user_id = param.user_id;
        this.address_id = param.address_id;
        this.quantity = param.quantity;
        this.total_price = param.total_price;
        this.payment_method = param.payment_method;
        this.createdAt =  param.createdAt,
        this.updatedAt = param.updatedAt
    }

    async viewOneTransaction(){
        const param = {"id": this.id};
        const result = await query.findOneTransaction(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }


    async viewAllTransactions(){
        const param = {};
        const result = await query.findAllTransactions(param);

        if(result.err){
            return result;
        }else{
            return wrapper.data(result.data);
        }
    }

}

module.exports = Transaction;