'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./command_model');
const command = require('./command');
const query = require('../queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const SNS = require('../../../../helpers/components/aws-sns/sns');
const Emitter = require('../../../../helpers/events/event_emitter');
const EventPublisher = require('../../../../helpers/events/event_publisher');

class Transaction{

    async addNewTransaction(payload){
        const data = [payload];
        let view = model.generalTransaction();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.item_id)){accumulator.item_id = value.item_id;}
            if(!validate.isEmpty(value.user_id)){accumulator.user_id = value.user_id;}
            if(!validate.isEmpty(value.address_id)){accumulator.address_id = value.address_id;}
            if(!validate.isEmpty(value.quantity)){accumulator.quantity = value.quantity;}
            if(!validate.isEmpty(value.total_price)){accumulator.total_price = value.total_price;}  
            if(!validate.isEmpty(value.payment_method)){accumulator.payment_method = value.payment_method;}
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneTransaction(document);
        return result;
    }

    async updateTransaction(params, payload){
        const data = [payload];
        let view = model.generalTransaction();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.item_id)){accumulator.item_id = value.item_id;}
            if(!validate.isEmpty(value.user_id)){accumulator.user_id = value.user_id;}
            if(!validate.isEmpty(value.address_id)){accumulator.address_id = value.address_id;}
            if(!validate.isEmpty(value.quantity)){accumulator.quantity = value.quantity;}
            if(!validate.isEmpty(value.total_price)){accumulator.total_price = value.total_price;}  
            if(!validate.isEmpty(value.payment_method)){accumulator.payment_method = value.payment_method;}        
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneTransaction(params, document);
        return result;
    }

    async deleteTransaction(params){
        const result = await command.deleteOneTransaction(params);
        return result;
    }

}

module.exports = Transaction;