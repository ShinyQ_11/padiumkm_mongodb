'use strict';

const generalTransaction = () => {
    const model = {
        id:``,
        item_id: ``,
        user_id: ``,
        address_id:``,
        quantity: ``,
        total_price: ``,
        payment_method: ``,
        createdAt:``,
        updatedAt:``,    
    }
    return model;
}

module.exports = {
    generalTransaction: generalTransaction
}
