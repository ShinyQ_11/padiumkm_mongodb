const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('../infra/configs/global_config');
const userQuery = require('../modules/user/repositories/queries/query');
const wrapper = require('../helpers/utils/wrapper');

const generateToken = async (payload) => {
    const privateKey = config.getPrivateKey();
    const verifyOptions = {
        issuer: 'telkomdev',
        expiresIn: '100m'
      };    

    const token = jwt.sign(payload, privateKey, verifyOptions);
    return token;
};

const getToken = (headers) => {
    if (headers && headers.authorization && headers.authorization.includes('Bearer')) {
      let parted = headers.authorization.split(' ');
      if (parted.length === 2) {
        return parted[1];
      }
    }
}

const verifyToken = async (req, res, next) => {
    const result = {
        data: null
    }
    const publicKey = config.getPrivateKey();
    const verifyOptions = {
        issuer: 'telkomdev'
    };

    const token = getToken(req.headers);

    if(!token){
        return wrapper.response(res, 'fail', result, 'Invalid token!', 403);
    }
    let decodedToken;

    try {
        decodedToken = jwt.verify(token, publicKey, verifyOptions);
        console.log(decodedToken);
    } catch (error) {
        console.log(error);
        if(error instanceof jwt.TokenExpiredError){
            return wrapper.response(res, 'fail', result, 'Access token expired!', 401);
        }else{
            return wrapper.response(res, 'fail', result, 'Token is not valid!', 401);
        }
    }

    const userId = decodedToken.sub;
    const user = userQuery.findById(userId);
    if(user.err){
        return wrapper.response(res, 'fail', result, 'Invalid token!', 403);
    }

    return userId;
}

module.exports = {
    generateToken,
    verifyToken
}